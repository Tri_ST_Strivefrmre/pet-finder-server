from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy 
from flask_marshmallow import Marshmallow 
from flask_cors import CORS, cross_origin
import os

# Initialize app
app = Flask(__name__)
CORS(app)

#setting up the base directory
basedir = os.path.abspath(os.path.dirname(__file__))

# Database config
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'petdb.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = True
# Initialize the db
db = SQLAlchemy(app)


ma = Marshmallow(app)

# Pet Class == Database Model
class Pet(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(100))
  image = db.Column(db.String(500))
  description = db.Column(db.String(200))
  pettype = db.Column(db.String(100))
  age = db.Column(db.Integer)
  status = db.Column(db.Boolean)

  def __init__(self, name, image ,description, pettype, age, status):
    self.name = name
    self.image = image
    self.description = description
    self.pettype = pettype
    self.age = age
    self.status = status

# Pet Schema - defining Pet Schema
class PetSchema(ma.Schema):
  class Meta:
    fields = ('id', 'name','image' ,'description', 'pettype', 'age', 'status')

# Initialize schema
pet_schema = PetSchema(strict=True)
pets_schema = PetSchema(many=True, strict=True)

# Create a Pet profile
@app.route('/pet', methods=['POST'])
def add_pet():
  name = request.json['name']
  image = request.json['image']
  description = request.json['description']
  pettype = request.json['pettype']
  age = request.json['age']
  status = request.json['status']

  new_pet = Pet(name, image,description, pettype, age, status)

  db.session.add(new_pet)
  db.session.commit()

  return pet_schema.jsonify(new_pet)

# Get All Pet details
@app.route('/pet', methods=['GET'])
def get_pets():
  all_pets = Pet.query.all()
  result = pets_schema.dump(all_pets)
  output = {'pets':result.data}
  return jsonify(output)

# Get All Pet details
@app.route('/lostpets', methods=['GET'])
def get_lost_pets():
  lost_pets = Pet.query.filter(Pet.status == True)
  result = pets_schema.dump(lost_pets)
  return jsonify(result.data)

# Get Single Pet with ID
@app.route('/pet/<id>', methods=['GET'])
def get_pet(id):
  pet = Pet.query.get(id)
  return pet_schema.jsonify(pet)

# Update a Product
@app.route('/pet/<id>', methods=['PUT'])
def update_pet_details(id):
  pet = Pet.query.get(id)

  name = request.json['name']
  image = request.json['image']
  description = request.json['description']
  pettype = request.json['pettype']
  age = request.json['age']
  status = request.json['status']


  pet.name = name
  pet.image = image
  pet.description = description
  pet.pettype = pettype
  pet.age = age
  pet.status = status

  db.session.commit()

  return pet_schema.jsonify(pet)

# Delete Product
@app.route('/pet/<id>', methods=['DELETE'])
def delete_pet(id):
  pet = Pet.query.get(id)
  db.session.delete(pet)
  db.session.commit()

  return pet_schema.jsonify(pet)

# Run Server
if __name__ == '__main__':
  app.run(host='0.0.0.0',debug=True)