## Server-side repository for Pet finder

### The application allows Pet owners advertise their missing pets on the website. The community can narrow down the searches according to their area.

### Once lost and found status of the Pet is updated on the website by the admin once notified by the Pet owner.

### Steps to get started:

#### 1. pip install pipenv
#### 2. git clone repository
#### 3. cd to the repository and run - pipenv install to install dependencies from PipFile
#### 4. Run the application - pipenv run python app.py
#### 5. git clone PetFinder-Client to run the client-side

#### For dependencies, check PipFile